package src.main.java;

import java.io.*;

class UnzipperTest
{
  public static void main(String[] args) throws Exception
  {
    AssertArgumentsCount(args.length);

    File
    txtDir              = new File(args[0]),
    zipDir              = new File(args[1]),
    targetDir           = new File(args[2]);

    AssertArgumentsValidity(txtDir, zipDir, targetDir);

    Unzipper unzipper = new Unzipper(txtDir, zipDir, targetDir, "MyLogger");
    unzipper.UnzipFiles();
  }

  private static void AssertArgumentsCount(int argc)
  {
    if (argc != 3)
    {
      System.out.println("ERRORCODE(1): You must set 3 arguments as input!");
      System.exit(1);
    }
  }

  private static void AssertArgumentsValidity(File txtDir, File zipDir, File targetDir)
  {
    StringBuffer errorMessage = new StringBuffer();
    
    if(!txtDir.isDirectory() ||
       !(txtDir.list().length == 1) ||
       !txtDir.list()[0].endsWith(".txt"))
    {
      errorMessage.append("The source directory of '.txt' file does not exist, " +
                          "or exist but does not contain exactly 1 '.txt' file!\n");
    }
    if(!zipDir.isDirectory())
    {
      errorMessage.append("The source directory of '.zip' files does not exist!\n");
    }
    if(!targetDir.isDirectory())
    {
      errorMessage.append("The target directory does not exist!\n");
    }

    if(errorMessage.length() > 0)
    {
      System.out.println("ERRORCODE(2):\n" + errorMessage.toString());
      System.exit(2);
    }
  }
}