package src.main.java;

import java.io.*;
import java.util.*;
import java.util.zip.*;
import java.util.logging.*;
import javafx.util.*;

class Unzipper
{
  private File                                 zipDir, targetDir, txtFile;
  private HashSet<String>                      txtEntries;
  private ArrayList<String>                    zipPaths;
  private HashMap<Integer, ArrayList<Integer>> txtEntriesLocations;
  private String                               logFilePath;
  private Logger                               logger;
  private String                               loggerName;

  public Unzipper(File txtDir, File zipDir, File targetDir, String loggerName) throws FileNotFoundException, IOException
  {
    this.zipDir     = zipDir;
    this.targetDir  = targetDir;
    this.loggerName = loggerName;
    
    txtFile = new File(txtDir + File.separator + txtDir.list()[0]);
    
    FillTxtEntries();
    FillZipPaths();
    FillTxtEntriesLocations();
    AssertTxtEntries();
  }

  private void FillTxtEntries() throws FileNotFoundException
  {
    txtEntries = new HashSet<String>();
    
    Scanner txtScanner = new Scanner(txtFile);
    while(txtScanner.hasNext())
    {
      txtEntries.add(txtScanner.next());
    }
  }

  private void FillZipPaths()
  {
    zipPaths = new ArrayList<String>();

    String[] zipNames = zipDir.list();
    for(String zipName : zipNames)
    {
      zipPaths.add(zipDir + File.separator + zipName);
    }
  }

  private void FillTxtEntriesLocations() throws IOException
  {
    txtEntriesLocations = new HashMap<Integer, ArrayList<Integer>>();

    int zipIndex = 0;
    while(zipIndex < zipPaths.size() && txtEntries.size() > 0)
    {
      ZipFile zipFile                            = new ZipFile(zipPaths.get(zipIndex));
      Enumeration zipEntries                     = zipFile.entries();
      ArrayList<Integer> locationsForThisZipFile = new ArrayList<Integer>();
      
      int entryNumber = 0;
      while(zipEntries.hasMoreElements() && txtEntries.size() > 0)
		  {
		  	ZipEntry zipEntry = (ZipEntry)zipEntries.nextElement();
        
        if(txtEntries.contains(zipEntry.getName()))
        {
          locationsForThisZipFile.add(entryNumber);
          txtEntries.remove(zipEntry.getName());
        }
        
        entryNumber += 1;
		  }
      
      txtEntriesLocations.put(zipIndex, locationsForThisZipFile);
      
      zipIndex += 1;
    }
  }
  
  private void AssertTxtEntries()
  {
    if(txtEntries.size() > 0)
    {
      ConfigureLogger();
      StringBuffer logMessage = new StringBuffer("ERRORCODE(3): The following files can't be " +
                                                                "found in the zip's:\n");
      for(String entry : txtEntries)
      {
        logMessage.append(" ==> " + entry);
      }
      logger.info(logMessage.toString());
      System.exit(3);
    }
  }

  public void UnzipFiles() throws IOException
  {    
    for(Map.Entry<Integer, ArrayList<Integer>> entryLocations : txtEntriesLocations.entrySet())
    {
      Integer zipIndex              = entryLocations.getKey();
      ArrayList<Integer> zipOffsets = entryLocations.getValue();

      ZipInputStream zipIn = new ZipInputStream(new FileInputStream(zipPaths.get(zipIndex)));

      ZipEntry entry  = zipIn.getNextEntry();
      int entryNumber = 0;
      for(Integer nextOffset : zipOffsets)
      {
        while(entry != null && entryNumber < nextOffset)
        {
          entry       = zipIn.getNextEntry();
          entryNumber += 1;
        }

        String filePathToUnzip = targetDir + File.separator + entry.getName();
        System.out.println("Extracting '" + entry.getName() + "' to '" + targetDir + "'...");

        UnzipFile(zipIn, filePathToUnzip);
        zipIn.closeEntry();

        System.out.println("Extraction of '" + entry.getName() + "' completed!\n");
      }

      zipIn.close();
    }
  }

  private void UnzipFile(ZipInputStream zipIn, String filePath) throws IOException
  {
    BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(filePath));
    byte[] bytesIn           = new byte[4096];
    int read                 = 0;
    while((read = zipIn.read(bytesIn)) != -1)
    {
      bos.write(bytesIn, 0, read);
    }
    bos.close();
  }

  private void ConfigureLogger()
  {
    logFilePath = targetDir + File.separator + "unzip_error.log";
    logger      = Logger.getLogger(loggerName);
    try
    {
      FileHandler fileHandler = new FileHandler(logFilePath);
      logger.addHandler(fileHandler);
      
      SimpleFormatter formatter = new SimpleFormatter();
      fileHandler.setFormatter(formatter);
    }
    catch(SecurityException e)
    {
      e.printStackTrace();
    }
    catch(IOException e)
    {
      e.printStackTrace();
    }
  }
}