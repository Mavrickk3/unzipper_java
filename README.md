Read the task's description below in hungarian:

    Egy paraméterként kapott elérési útvonalon található könyvtárban rendelkezésre állnak zip állományok,
    amelyekben véletlenszerűen vannak tömörítve különböző fájlok.
    Mind a zip-ek száma, mind a bennük lévő fájlok mérete, mind a számossága véletlenszerű.
    A zip-ek nem tartalmaznak mappákat, más zip-eket, flat-ként tekinthetünk rájuk.
    Minden fájl csak egyszer szerepelhet a zip-ekben, nem kell duplikációra készülni.
    
    Egy másik, szintén paraméterként kapott elérési útvonalon található könyvtárban rendelkezésre áll egy txt fájl.
    Ebben soronként tördelve a zip-ekben lévő fájlok egy részhalmaza van listázva.
    Ha nincs ott a fájl, vagy több txt fájl van a könyvtárban, a program álljon le hibajelzéssel.
    Ez a fájl csak irányítja a másolást, azt átmásolni nem szükséges.
    
    Készítsen egy konzol alkalmazást Java nyelven,
    amely a zip-ekből egy paraméterként megkapott célkönyvtárba csomagolja ki a txt-ben felsorolt fájlokat, de csak az ott felsoroltakat!
    A megvalósítás során készüljön nagy méretű állományokra és a nagy számosságra is,
    így a performanciát a bejárások során tartsa szem előtt. Ha szükséges, készüljön unit teszt is.
    A program készítése során figyeljen az esetleges hibákra, például,
    ha nem található a zip-ekben egy, a txt-ben megadott fájl.
    A program naplózza a működését és hiba esetén készítsen egy hibafájlt a célkönyvtárba,
    amely segítségével a hiba oka minél könnyebben megtalálható.
    A hibafájl neve unzip_error.log legyen. Hiba esetén ne futtassa le a kicsomagolást!
    
    A feladat része továbbá egy gradle build fájl elkészítése is,
    ami a forrásból egy futtatható, standalone unzipTest.jar nevű fájlt hoz létre.
    
    A jar fájl futtatható legyen az alábbi formátumú parancs kiadásával:
    > java –jar unzipTest.jar [source_txt_dir] [source_zip_dir] [target_dir]
    Itt a program paraméterként a következőket fogja megkapni: 
    [source_txt_dir] – txt fájlt tartalmazó könyvtár
    [source_zip_dir] – zip fájlokat tartalmazó könyvtár
    [target_dir] – célkönyvtár, ide kell kicsomagolni a szükséges fájlokat
    
    Beadás formátuma
    Csak a program forráskódját és a hozzá készült build.gradle fájlt (gradle 4.x javasolt) kérjük beadni,
    egy darab zip fájlban. A zip fájl belső struktúrája a következő legyen:
    [projekt_könyvtár]
    	src/main/java
    	src/main/test
    	build.gradle

To build and run with example parameters the program, run java_unzipper/test.bat.
To build and create JAR outuput with gradle run "gradle jar" command from console when you are in java_unzipper directory.
After this, you can run the program from java_unzipper/build/libs direcotry with the command 'java -jar unzipTest.jar [sourceTxtDir] [sourceZipDir] [targetDir]'